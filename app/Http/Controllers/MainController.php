<?php

namespace App\Http\Controllers;

class MainController extends Controller
{
    public function home()
    {
        $current_page = 'home';
        
        return view('front-end.pages.home', compact('current_page'));
    }

    public function about_us()
    {
        $current_page = 'about-us';
        
        return view('front-end.pages.about-us', compact('current_page'));
    }

    public function courses()
    {
        $current_page = 'courses';
        
        return view('front-end.pages.courses', compact('current_page'));
    }

    public function contact()
    {
        $current_page = 'contact';
        
        return view('front-end.pages.contact', compact('current_page'));
    }
}