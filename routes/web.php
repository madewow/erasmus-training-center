<?php

Route::get('/', 'MainController@home');
Route::get('/about-us', 'MainController@about_us');
Route::get('/courses', 'MainController@courses');
Route::get('/contact', 'MainController@contact');
