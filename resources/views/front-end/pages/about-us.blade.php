@extends('front-end.templates._base')

@push('page-meta-tags')
<title>About Us - Erasmus Training Center</title>
@endpush

@push('body-class')
<body id="about-us-page">
@endpush

@section('content')
<section class="hero-image" data-interchange="[{{ asset('assets/img/hero-image-home-large.jpg') }}, xsmall], [{{ asset('assets/img/hero-image-home-large.jpg') }}, medium], [{{ asset('assets/img/hero-image-home-large.jpg') }}, large], [{{ asset('assets/img/hero-image-home-large.jpg') }}, xlarge]">
	<div class="grid-container">
		<div class="grid-x">
		</div>
	</div>
</section>

<div class="grid-container page-title">
	<div class="grid-x grid-margin-x large-margin-collapse">
		<div class="cell large-12">
			<h1>TRAINING CENTRE &amp; MEETING PLATFORM</h1>
		</div>
	</div>

	<div class="grid-x grid-margin-x large-margin-collapse">
		<div class="cell large-12 text-container">
			<div class="grid-x grid-padding-x">
				<div class="cell large-6">
					<p>Erasmus Training Centre (ETC), formally known as the Erasmus Taal Centrum, based in Jakarta, provides skill training and language courses to enhance student &amp; academic mobility between the Netherlands and Indonesia. ETC is also serves as a platform bringing Dutch universities and Indonesian professionals together through joint workshops, seminars and lectures.</p>
				</div>

				<div class="cell large-6">
					<p>The centre was re-opened officially on 13 February 2017 by the Dutch Minister of Education, Jet Bussemaker and initiated by 4 Dutch universities (Leiden, Groningen, VU Amsterdam and Stenden) as well as the Embassy of the Kingdom of the Netherlands in Indonesian and Nuffic NESO Indonesia</p>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="grid-container panel-container">
	<div class="grid-x grid-padding-x">
		<div class="cell xsmall-12 large-6">
			<div class="panel">
				<img src="{{ asset('assets/img/panel-courses-dutch.jpg') }}">

				<div class="title-bar teal">
					<h1>Our Students</h1>
				</div>

				<div class="text-container">
					<p>Our students come from all over Indonesia, but if you are an interested language lover or you need to be prepared for your studies in the Netherlands, we provide you with a challenging environment. It’s not just a choice for a study, but being part of a community for years to come.</p>
				</div>
			</div>
		</div>

		<div class="cell xsmall-12 large-6">
			<div class="panel">
				<img src="{{ asset('assets/img/panel-courses-dutch.jpg') }}">

				<div class="title-bar light-blue">
					<h1>Our Teachers</h1>
				</div>

				<div class="text-container">
					<p>Our teaching team  is composed  of professional teacher who are certified in teaching language, espcially Dutch language. They are all alumni of Dutch literature graduates form various leading universities in Indonesia as well as universities in the Netherlands.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-8">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/about-us-classroom-03.jpg') }}">
			</div>
		</div>

		<div class="cell xsmall-12 large-4">
			<div class="panel" data-equalizer-watch>
				<div class="title-bar orange">
					<h1>
						Our Courses
						<i class="fas fa-arrow-right"></i>
					</h1>
				</div>

				<div class="text-container">
					<p>Our students come from all over Indonesia, but if you are an interested language lover or you need to be prepared for your studies in the Netherlands, we provide you with a challenging environment. It’s not just a choice for a study, but being part of a community for years to come.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-4 xsmall-order-2 large-order-1">
			<div class="panel" data-equalizer-watch>
				<div class="title-bar teal">
					<h1>Our Facilities</h1>
				</div>
			</div>
		</div>

		<div class="cell xsmall-12 large-8 xsmall-order-1 large-order-2">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/about-us-classroom-03.jpg') }}">
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x">
		<div class="cell xsmall-12 large-6">
			<img src="{{ asset('assets/img/about-us-classroom-05.jpg') }}">
		</div>

		<div class="cell xsmall-12 large-6">
			<img src="{{ asset('assets/img/about-us-classroom-05.jpg') }}">
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-8">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/about-us-etc-benefits.jpg') }}">
			</div>
		</div>

		<div class="cell xsmall-12 large-4">
			<div class="panel" data-equalizer-watch>
				<div class="title-bar blue">
					<img src="{{ asset('assets/img/text-etc-benefits.png') }}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
@endpush