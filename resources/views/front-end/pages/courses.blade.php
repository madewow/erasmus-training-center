@extends('front-end.templates._base')

@push('page-meta-tags')
<title>Our Courses - Erasmus Training Center</title>
@endpush

@push('body-class')
<body id="about-us-page">
@endpush

@section('content')
<section class="hero-image" data-interchange="[{{ asset('assets/img/hero-image-home-large.jpg') }}, xsmall], [{{ asset('assets/img/hero-image-home-large.jpg') }}, medium], [{{ asset('assets/img/hero-image-home-large.jpg') }}, large], [{{ asset('assets/img/hero-image-home-large.jpg') }}, xlarge]">
	<div class="grid-container">
		<div class="grid-x">
		</div>
	</div>
</section>

<div class="grid-container page-title">
	<div class="grid-x grid-margin-x large-margin-collapse">
		<div class="cell large-12">
			<h1>AN OVERVIEW OF OUR COURSES</h1>
		</div>
	</div>
</div>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-7">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/courses-course-dutch.jpg') }}">
			</div>
		</div>

		<div class="cell xsmall-12 large-5">
			<div class="panel">
				<div class="grid-y" data-equalizer-watch>
					<div class="cell large-shrink">
						<div class="panel">
							<div class="title-bar orange">
								<h1>Dutch <span>Language &amp; Culture</span></h1>
							</div>

							<div class="text-container">
								<p>Learn Dutch in a simple way and fun at Erasmus Training Centre! Good to know the culture through its language? Why not? The more languages you speak , the bigger your world becomes! So take your Dutch journery with our regular program that open 3 times in a year!</p>
							</div>
						</div>
					</div>

					<div class="cell large-auto grid-x align-bottom xsmall-12">
						<ul class="courses-list">
							<li>
								<a>Course 1</a>
							</li>

							<li>
								<a>Course 2</a>
							</li>

							<li class="register">
								<a>
									Register
									<i class="fas fa-file-pdf"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-5 xsmall-order-2 large-order-1">
			<div class="panel">
				<div class="grid-y" data-equalizer-watch>
					<div class="cell large-shrink">
						<div class="panel">
							<div class="title-bar purple">
								<h1>Indonesian <span>Language &amp; Culture</span></h1>
							</div>

							<div class="text-container">
								<p>Learn Dutch in a simple way and fun at Erasmus Training Centre! Good to know the culture through its language? Why not? The more languages you speak , the bigger your world becomes! So take your Dutch journery with our regular program that open 3 times in a year!</p>
							</div>
						</div>
					</div>

					<div class="cell large-auto grid-x align-bottom xsmall-12">
						<ul class="courses-list">
							<li>
								<a>Course 1</a>
							</li>

							<li>
								<a>Course 2</a>
							</li>

							<li class="register">
								<a>
									Register
									<i class="fas fa-file-pdf"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="cell xsmall-12 large-7 xsmall-order-1 large-order-2">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/courses-course-dutch.jpg') }}">
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-7">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/courses-course-dutch.jpg') }}">
			</div>
		</div>

		<div class="cell xsmall-12 large-5">
			<div class="panel">
				<div class="grid-y" data-equalizer-watch>
					<div class="cell large-shrink">
						<div class="panel">
							<div class="title-bar blue">
								<h1>Academic <span>Skills &amp; Preperations</span></h1>
							</div>

							<div class="text-container">
								<p>Learn Dutch in a simple way and fun at Erasmus Training Centre! Good to know the culture through its language? Why not? The more languages you speak , the bigger your world becomes! So take your Dutch journery with our regular program that open 3 times in a year!</p>
							</div>
						</div>
					</div>

					<div class="cell large-auto grid-x align-bottom xsmall-12">
						<ul class="courses-list">
							<li>
								<a>Course 1</a>
							</li>

							<li>
								<a>Course 2</a>
							</li>

							<li class="register">
								<a>
									Register
									<i class="fas fa-file-pdf"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
@endpush