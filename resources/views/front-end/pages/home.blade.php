@extends('front-end.templates._base')

@push('page-meta-tags')
<title>Welcome to Erasmus Training Centre</title>
@endpush

@push('body-class')
<body id="home-page">
@endpush

@section('content')
<section class="hero-image" data-interchange="[{{ asset('assets/img/hero-image-home-large.jpg') }}, xsmall], [{{ asset('assets/img/hero-image-home-large.jpg') }}, medium], [{{ asset('assets/img/hero-image-home-large.jpg') }}, large], [{{ asset('assets/img/hero-image-home-large.jpg') }}, xlarge]">
	<div class="grid-container">
		<div class="grid-x align-right align-middle">
			<img src="{{ asset('assets/img/badge-be-smart-get-ready.png') }}" id="badge-be-smart-get-ready">
		</div>
	</div>
</section>

<div class="grid-container page-title">
	<div class="grid-x grid-margin-x xsmall-margin-collapse">
		<div class="cell auto">
			<h1>WELCOME TO THE ERASMUS TRAINING CENTRE</h1>
		</div>

		<div class="cell shrink">
			<a class="cta">
				<span>REGISTER NOW!</span>
				<i class="fas fa-arrow-right"></i>
			</a>
		</div>
	</div>
</div>

<section class="panel-container grid-container">
	<div class="grid-x">
		<div class="panel">
			<img src="{{ asset('assets/img/panel-courses-dutch.jpg') }}">

			<div class="title-bar orange">
				<h1>Dutch
					<span>Language &amp; Culture</span>

					<i class="fas fa-arrow-right"></i>
				</h1>
			</div>
		</div>
	</div>

	<div class="grid-x grid-padding-x">
		<div class="cell xsmall-12 large-6">
			<div class="panel">
				<img src="{{ asset('assets/img/panel-courses-dutch.jpg') }}">

				<div class="title-bar purple">
					<h1>Indonesian
						<span>Language &amp; Culture</span>

						<i class="fas fa-arrow-right"></i>
					</h1>
				</div>
			</div>
		</div>

		<div class="cell xsmall-12 large-6">
			<div class="panel">
				<img src="{{ asset('assets/img/panel-courses-dutch.jpg') }}">

				<div class="title-bar blue">
					<h1>Academic
						<span>Skill &amp; Preparation</span>

						<i class="fas fa-arrow-right"></i>
					</h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="five-reasons grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell">
			<div class="cell text-center">
				<h1>5 REASONS WHY TO CHOOSE ETC:</h1>
			</div>
		</div>

		<div class="cell">
			<div id="carousel" class="grid-x grid-margin-x" data-equalizer data-equalizer-on="medium">
				<div class="cell xsmall-12 large-auto">
					<div class="box" data-equalizer-watch>
						<img src="{{ asset('assets/img/badge-reason-01.png') }}" class="badge">

						<p class="title">30,000+<br>STUDENTS</p>

						<p>Successful graduation track record since 1981.</p>
					</div>
				</div>

				<div class="cell xsmall-12 large-auto">
					<div class="box" data-equalizer-watch>
						<img src="{{ asset('assets/img/badge-reason-02.png') }}" class="badge">

						<p class="title">20+<br>TEACHERS</p>

						<p>High qualified local and native teachers.</p>
					</div>
				</div>

				<div class="cell xsmall-12 large-auto">
					<div class="box" data-equalizer-watch>
						<img src="{{ asset('assets/img/badge-reason-03.png') }}" class="badge">

						<p class="title">PARTNER-<br>SHIPS</p>

						<p>Easy access to Dutch higher education.</p>
					</div>
				</div>

				<div class="cell xsmall-12 large-auto">
					<div class="box" data-equalizer-watch>
						<img src="{{ asset('assets/img/badge-reason-04.png') }}" class="badge">

						<p class="title">COURSE<br>OFFER</p>

						<p>Varied, specialized and tailor-made course programs.</p>
					</div>
				</div>

				<div class="cell xsmall-12 large-auto">
					<div class="box" data-equalizer-watch>
						<img src="{{ asset('assets/img/badge-reason-05.png') }}" class="badge">

						<p class="title">MEETING<br>PLATFORM</p>

						<p>Between Indonesian &amp; Dutch academics, universities and institutions.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="grid-container panel-container" id="etc-tv">
	<div class="grid-x grid-margin-x large-margin-collapse" data-equalizer>
		<div class="cell xsmall-12 large-3 xsmall-order-2 large-order-1">
			<div class="panel" data-equalizer-watch>
				<div class="title-bar teal">
					<h1>ETC TV</h1>
				</div>
			</div>
		</div>

		<div class="cell xsmall-12 large-9 xsmall-order-1 large-order-2">
			<div class="panel" data-equalizer-watch>
				<img src="{{ asset('assets/img/home-etc-tv.jpg') }}">
			</div>
		</div>
	</div>
</section>
@endsection

@push('page-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
@endpush

@push('page-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

<script>
	$('#carousel').slick({
		infinite: false,
		arrows: false,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: true,
		responsive: [
			{
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
			{
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		],
	});
</script>
@endpush