@extends('front-end.templates._base')

@push('page-meta-tags')
<title>About Us - Erasmus Training Center</title>
@endpush

@push('body-class')
<body id="about-us-page">
@endpush

@section('content')
<section class="hero-image" data-interchange="[{{ asset('assets/img/hero-image-home-large.jpg') }}, xsmall], [{{ asset('assets/img/hero-image-home-large.jpg') }}, medium], [{{ asset('assets/img/hero-image-home-large.jpg') }}, large], [{{ asset('assets/img/hero-image-home-large.jpg') }}, xlarge]">
	<div class="grid-container">
		<div class="grid-x">
		</div>
	</div>
</section>

<div class="grid-container page-title">
	<div class="grid-x grid-margin-x large-margin-collapse">
		<div class="cell large-12">
			<h1>Contact Us</h1>
		</div>
	</div>
</div>

<section class="grid-container full">
	<div id="gmap" class="grid-x">

	</div>
</section>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJIH7Ywy7JzOcsm9NhbpiRnCraSx5Dzk"></script>

<script>
	function initialize_gmap(el) {
	    var mapLatlang = new google.maps.LatLng(-6.2343499,106.8265362);
	    
	    var mapOptions = {
	        center: mapLatlang,
	        zoom: 15,
	        scrollwheel: false,
			navigationControl: false,
			mapTypeControl: false,
			scaleControl: false,
			draggable: false,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]
	        };

	    var map = new google.maps.Map(document.getElementById(el),  mapOptions);

	    var marker = new google.maps.Marker({
		    position: mapLatlang,
		    map: map,
		  });
	}

	$(document).ready(function() {
		initialize_gmap("gmap");
	});
</script>
@endpush