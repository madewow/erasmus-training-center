<li @if($current_page == "home") class="active" @endif>
	<a href="{{ url('/') }}">Home</a>
</li>

<li @if($current_page == "about-us") class="active" @endif>
	<a href="{{ url('/about-us') }}">About Us</a>
</li>

<li @if($current_page == "courses") class="active" @endif>
	<a href="{{ url('/courses') }}">Courses</a>
</li>

<li @if($current_page == "contact") class="active" @endif>
	<a href="{{ url('/contact') }}">Contact</a>
</li>
