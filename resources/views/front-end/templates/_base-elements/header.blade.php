<header id="main-header">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-10 large-auto">
				<img src="{{ asset('assets/img/logo-etc-header.png') }}">
			</div>

			<div class="cell large-7 hide-for-small-only hide-for-medium-only show-for-large">
				<div class="grid-y">
					<div class="cell large-6 text-right">
						<ul class="language-navigation">
							<li>
								<a>Eng</a>
							</li>

							<li>&nbsp;&bullet;&nbsp;</li>

							<li>
								<a>ID</a>
							</li>
						</ul>
					</div>

					<div class="cell large-6 grid-x align-bottom">
						<ul class="main-navigation">
							@include('front-end.templates._base-elements.main-navigation')
						</ul>

						<ul class="share-navigation">
							@include('front-end.templates._base-elements.share-navigation')
						</ul>
					</div>
				</div>
			</div>

			<button data-toggle="off-canvas" class="hide-for-large">
				<i class="fas fa-bars"></i>
			</button>
		</div>
	</div>
</header>