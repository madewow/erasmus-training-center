<ul class="language-navigation">
	<li>
		<a>Eng</a>
	</li>

	<li>&nbsp;&bullet;&nbsp;</li>

	<li>
		<a>ID</a>
	</li>
</ul>

<ul class="main-navigation">
	@include('front-end.templates._base-elements.main-navigation')
</ul>

<div class="text-center">
	<ul class="share-navigation">
		@include('front-end.templates._base-elements.share-navigation')
	</ul>
</div>