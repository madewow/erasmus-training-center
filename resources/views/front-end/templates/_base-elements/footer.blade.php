<footer id="main-footer">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-3 xsmall-order-1">
				<div class="grid-y">
					<div class="cell xsmall-12 large-6">
						<img src="{{ asset('assets/img/logo-etc-footer.png') }}" class="logo">
					</div>

					<div class="cell grid-x align-bottom xsmall-12 large-auto">
						<p class="copyright show-for-large">&copy; 2018 - Erasmus Training Centre<br>Yayasan Bahasa dan Edukasi Belanda Erasmus</p>
					</div>
				</div>
			</div>

			<div class="cell xsmall-12 large-3 xsmall-order-2">
				<address>Royal Netherlands Embassy<br>Jl. H.R. Rasuna Said Kav. S-3<br>Jakarta 12950, Indonesia</address>

				<address><a>info@erasmustrainingcentre.com</a><br>Mobile +62 852 1780 1660<br>Telephone +62 21 525 05 07<br>Fax +62 21 525 03 79</address>
			</div>

			<div class="cell xsmall-12 large-2 large-order-3 xsmall-order-4">
				<p class="navigation-title hide-for-xsmall-only show-for-large">PAGES<span>:</span></p>

				<ul class="main-navigation hide-for-xsmall-only hide-for-small-only hide-for-medium-only">
					@include('front-end.templates._base-elements.main-navigation')
				</ul>

				<div class="t-c-container">
					<a class="t-c">
						<i class="far fa-file-pdf"></i>&nbsp;
						<span>Official Statement</span>
					</a>

					<a class="t-c">
						<i class="far fa-file-pdf"></i>&nbsp;
						<span>Terms &amp; Conditions</span>
					</a>
				</div>

				<p class="copyright show-for-xsmall text-center hide-for-large">&copy; 2018 - Erasmus Training Centre Yayasan Bahasa dan Edukasi Belanda Erasmus</p>
			</div>

			<div class="cell xsmall-12 large-4 xsmall-order-3 large-order-4">
				<div class="grid-y">
					<div class="cell xsmall-12 large-6">
						<img src="{{ asset('assets/img/text-hope-to-see-you-soon.png') }}" class="text-hope-to-see-you-soon">
					</div>

					<div class="cell grid-x align-bottom xsmall-12 large-auto">
						<div class="share-container">
							<p>Follow us</p>

							<ul class="share-navigation">
								@include('front-end.templates._base-elements.share-navigation')
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>